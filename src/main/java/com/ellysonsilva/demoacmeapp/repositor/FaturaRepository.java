package com.ellysonsilva.demoacmeapp.repositor;
import java.util.List;
import java.util.Optional;

import com.ellysonsilva.demoacmeapp.domain.Fatura;
import com.ellysonsilva.demoacmeapp.domain.Instalacao;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FaturaRepository extends JpaRepository<Fatura, Long> {

    public Optional<Fatura> findByCodigo(String codigo);
    public List<Fatura> findByInstalacao(Instalacao instalacao);

}