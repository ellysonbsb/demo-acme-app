package com.ellysonsilva.demoacmeapp.repositor;

import java.util.Optional;

import com.ellysonsilva.demoacmeapp.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ClienteRepository extends JpaRepository<Cliente, Long> {

    public Optional<Cliente> findByCpf(String cpf);

}