package com.ellysonsilva.demoacmeapp.repositor;
import java.util.List;
import java.util.Optional;

import com.ellysonsilva.demoacmeapp.domain.Cliente;
import com.ellysonsilva.demoacmeapp.domain.Instalacao;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InstalacaoRepository extends JpaRepository<Instalacao, Long> {

    public Optional<Instalacao> findByCodigo(String codigo);
    public List<Instalacao> findByCliente(Cliente cliente);


}